def sum(x,y):
    sum = x + y
    if sum in range(15,20):
        return 20
    else:
        return sum

print(sum(10,6))
print(sum(10,2))
print(sum(10,12))

# The defined function calculate the sum of two input arguments. If this sum in the range from 15 to 20, it outputs
# 20, otherwise it output the sum
# With two arguments (10,6) the sum is 16 and in the range(15,20) thus print(sum(10,6)) print out the value of 20
# with (10,2) and (10,12) the sums for each couple are 12 and 22, respectively which are not in the range from 15 to 20,
# thus we have two outputs 12 and 22.