import datetime
now = datetime.datetime.now()
print("Current date and time : ")
print(now.strftime("%Y-%m-%d %H:%M:%S"))

# This code return the current datetime and output to stdout in the format %Y-%m-%d %H:%M:%S