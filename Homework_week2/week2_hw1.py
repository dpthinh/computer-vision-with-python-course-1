# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 10:30:13 2018

@author: ThinhDo
"""
# This program get a list of numbers from user then output list of even and
# odd numbers

data = input("Give me a list of numbers ")
nums = list(map(int,data.split(" ")))
odd_list = []
even_list = []
for num in nums:
    if num % 2 == 0:
        even_list.append(num)
    else:
        odd_list.append(num)
        
print("even list: ", even_list)
print("odd list: ", odd_list)
    