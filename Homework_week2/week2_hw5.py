# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 18:26:05 2018

@author: ThinhDo
"""

# type your input matrix here
mat = [[1,2,3,4,5], [3,4,2,5,6], [1,6,3,2,5]]

num_row = len(mat)
num_col = len(mat[0])

result = 0
for i in range(num_row):
    result += sum(mat[i])
    
print('sum of matrix: ',result)