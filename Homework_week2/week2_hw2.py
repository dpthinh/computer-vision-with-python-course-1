# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 12:42:19 2018

@author: ThinhDo
"""

data1 = input("Give me your first day (dd/mm/yyyy): ")
data2 = input("Give me your second day (dd/mm/yyyy): ")
[date1,month1,year1] = list(map(int,data1.split("/")))
[date2,month2,year2] = list(map(int,data2.split("/")))

ref_year = min(year1,year2)

accum1 = [0,31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]
accum2 = [0,31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335]

ref_year = min(year1,year2)

# count how many years in range min(year1,year2) -- year1 are "special"
count1 = 0
for value in range(ref_year,year1):
    if (value % 4 == 0 and value % 100 != 0) or (value % 400 == 0):
        count1 += 1
        
# check whether the year1 has 29 or 28 days in Feb
if (year1 % 4 == 0 and year1 % 100 != 0) or (year1 % 400 == 0):
    tmp1 = date1 + accum2[month1-1] + 365*(year1-ref_year-count1) + 366*count1
else:
    tmp1 = date1 + accum1[month1-1] + 365*(year1-ref_year-count1) + 366*count1
    
# count how many years in range min(year1,year2) -- year2 are "special"
count2 = 0
for value in range(ref_year,year2):
    if (value % 4 == 0 and value % 100 != 0) or (value % 400 == 0):
        count2 += 1
 
# check whether the year2 has 29 or 28 days in Feb       
if (year2 % 4 == 0 and year2 % 100 != 0) or (year2 % 400 == 0):
    tmp2 = date2 + accum2[month2-1] + 365*(year2-ref_year-count2) + 366*count2
else:
    tmp2 = date2 + accum1[month2-1] + 365*(year2-ref_year-count2) + 366*count2
    
# Output the value
print('Number of days among two input date',max(tmp1,tmp2) - min(tmp1,tmp2))
        
        




    