# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 18:58:03 2018

@author: ThinhDo
"""
import numpy as np
N = int(input("Input N: "))
myarr = [[[np.random.uniform(0,10) for _ in range(N)] for _ in range(N)] for _ in range(N)]

print(myarr)
##print(myarr[:,:,0])

print('Min of array by 3 axis')
print(np.amin(myarr,axis=0))
print(np.amin(myarr,axis=1))
print(np.amin(myarr,axis=2))

print('\n')
print('Max of array by 3 axis')
print(np.amax(myarr,axis=0))
print(np.amax(myarr,axis=1))
print(np.amax(myarr,axis=2))

print('\n')
print('Sum of array by 3 axis')
print(np.sum(myarr,axis=0))
print(np.sum(myarr,axis=1))
print(np.sum(myarr,axis=2))


