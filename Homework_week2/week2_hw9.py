# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 19:53:11 2018

@author: ThinhDo
"""

import numpy as np
myarr = [[np.random.uniform(-10,10) for _ in range(8)] for _ in range(10)]
#print(myarr)

num_row = len(myarr)
num_col = len(myarr[0])

num = int(input("Give me a number: "))

tmp = max(myarr[0][0] - num,num-myarr[0][0])
result = myarr[0][0]
for idx1 in range(num_row):
    for idx2 in range(num_col):
        if max(myarr[idx1][idx2]-num,num-myarr[idx1][idx2]) < tmp:
            tmp = max(myarr[idx1][idx2]-num,num-myarr[idx1][idx2])
            result = myarr[idx1][idx2]
            
print("The closest value to your input is: ", result)


    