# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 18:29:56 2018

@author: ThinhDo
"""

# 1. Let's create a numpy array from a list
#  a. Import the "numpy" library as "np".
import numpy as np

#  b. Create a list with values 1 to 10 and assign it to the variable "x"
x = [i for i in range(1,11)]

#  c. Create an integer array from "x" and assign it to the variable "a1"
a1 = np.array(x, 'i')
print(a1)

#  d. Create an array of floats from "x" and assign it to the variable "a2"
a2 = np.array(x, 'f')
print(a2)

#  e. Print the data type of each array
print('Data type of a1: ',a1.dtype)
print('Date type of a2: ',a2.dtype)

# 2. Let's create arrays in different ways.
#  a. Create an array of shape (2,3,4) of zeros
a = np.zeros((2,3,4))
print(a)

#  b. Create an array of shape (2,3,4) of ones.
a = np.ones((2,3,4))
print(a)

#  c. Create an array with values 0 to 999 using the "np.arrange"
a = np.arange(1,1000)
print(a)

# 3. Let's look at indexing and slicing arrays.
#  a. Create an array from the list [2, 3.2, 5.5, -6.4, -2.2, 2.4] and assign it to the variable "a"
a = np.array([2, 3.2, 5.5, -6.4, -2.2, 2.4])

#  b. Do you know what a[1] will equal? Print it to see
print(a[1])

#  c. Try printing a[1:4] to see what the equals
print(a[1:4])

#  d. Create a 2-D array
a = np.array([[2, 3.2, 5.5, -6.4, -2.2, 2.4],\
              [1, 22, 4, 0.1, 5.3, -9],\
              [3, 1, 2.1, 21, 1.1, -2]])
#print(b)

#  e.
print(a[:,3])
print(a[1:4, 0:4])
print(a[1:,2])



























