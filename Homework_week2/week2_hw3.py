# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 16:01:01 2018

@author: ThinhDo
"""
import numpy as np
import matplotlib.pyplot as plt
N = int(input("Input N: "))
x = np.random.randint(0,11,N)
plt.hist(x,bins=20)
plt.show()

# print statistics
print('max: ',max(x))
print('min: ',min(x))
print('mean: ',np.mean(x))
print('variance: ',np.var(x))
print('standard deviation: ', np.std(x))

print('lists before removing duplicates: ',x)

result = []
for value in x:
    if value not in result:
        result.append(value)
print('lists after removing duplicates: ',result)


