# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 19:44:23 2018

@author: ThinhDo
"""

import numpy as np
M = int(input("Input M: "))
N = int(input("Input N: "))
K = int(input("Input K: "))
myarr = [[[np.random.randint(0,10) for _ in range(N)] for _ in range(M)] for _ in range(K)]
print(myarr)
##print(myarr[:,:,0])

print('Min of array by 3 axis')
print(np.amin(myarr,axis=0))
print(np.amin(myarr,axis=1))
print(np.amin(myarr,axis=2))

print('\n')
print('Max of array by 3 axis')
print(np.amax(myarr,axis=0))
print(np.amax(myarr,axis=1))
print(np.amax(myarr,axis=2))

print('\n')
print('Sum of array by 3 axis')
print(np.sum(myarr,axis=0))
print(np.sum(myarr,axis=1))
print(np.sum(myarr,axis=2))